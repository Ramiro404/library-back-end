
package com.ramir.library.domain;

import java.io.Serializable;
import java.sql.Date;
import java.time.Year;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;

/**
 *
 * @author ramir
 */
@Data

public class Edition implements Serializable{
    public static final long serialVersionUID = 1L;
    

    private Long idEdition;
    private String isbn;
    private String name;
    private Year year;
    

    private Book book;
    
    /*
    @OneToMany(mappedBy = "edition")
    private List<Language> languages;*/
    

    private Set<Language> editionLanguages;
    
    private List<Copy> copies;
}
