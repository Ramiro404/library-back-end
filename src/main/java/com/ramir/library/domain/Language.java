
package com.ramir.library.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;
/**
 *
 * @author ramir
 */
@Data

public class Language implements Serializable{
    public static final long serialVersionUID = 1L;
    

    private Long idLanguage;
    private String name;
    

    private Edition edition;
    
    private Set<Edition> editions;
    
}
