/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.domain;

/**
 *
 * @author ramir
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import lombok.Data;

@Data
public class User implements Serializable{
    
    public static final Long serialVersionUID = 1L;
    //@GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("dni_user")
    private Long dniUser;   
    private String name;
    @JsonProperty("last_name")
    private String lastName;
    private String email;
    private String password;
    
}
