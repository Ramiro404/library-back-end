/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.repository;

import com.ramir.library.dao.UserDao;
import com.ramir.library.domain.User;
import com.ramir.library.mappers.UserMapper;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ramir
 */
@Repository
public class UserRepository implements UserDao{
    private JdbcTemplate jdbcTemplate;

    public UserRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    @Override
    public List<User> getAllUsers(){
        return jdbcTemplate.query("select * from users", new UserMapper());
    }
    
    @Override
    public User getUserById(Long idUser){
        String SQL = "select * from users where dni_user=?";
        return jdbcTemplate.queryForObject(SQL, new UserMapper(), idUser);
    }
    
    @Override
    public User updateUser(User user){
        String SQL = "update users set name=?,last_name=?,email=?,password=? where dni_user=?";
        jdbcTemplate.update(SQL, user.getName(), user.getLastName(), user.getEmail(), user.getPassword(), user.getDniUser());
        return getUserById(user.getDniUser());
    }
    
    @Override
    public void createUser(User user){
        String SQL = "insert into users(name,last_name,email,password) values(?,?,?,?)";
        jdbcTemplate.update(SQL, user.getName(),user.getLastName(), user.getEmail(),user.getPassword());
    }
    
    @Override
    public void deleteUser(Long idUser){
        String SQL= "delete from users where dni_user=?";
        jdbcTemplate.update(SQL, idUser);
    }
    
    
}
