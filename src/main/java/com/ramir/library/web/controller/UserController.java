/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.web.controller;
import com.ramir.library.domain.User;
import com.ramir.library.repository.UserRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
/**
 *
 * @author ramir
 */
@RestController
@RequestMapping(value = "/api")
public class UserController {
    @Autowired
    private UserRepository userRepository;
    //private UserService userService;
    
    @GetMapping("/users")
    public List<User> getUsers(){
        //return userService.getUsers();
        List<User> users = userRepository.getAllUsers();
        return users;
    }
    
    @GetMapping("users/{id}")
    public User getUserById(@PathVariable(value = "id") Long idUser){
        //return userService.findUserById(idUser);
        User user = userRepository.getUserById(idUser);
        return user;
    }
    
    @PostMapping("/users")
    public void createUser(@Valid @RequestBody User user){
        //userService.saveUser(idUser);
        userRepository.createUser(user);
    }
    
     @RequestMapping(value = "/create-user", method = RequestMethod.POST)
    public ResponseEntity<User> create(@Valid @RequestBody User user) {
        userRepository.createUser(user);
        return new ResponseEntity(user, HttpStatus.CREATED);
    }
    
    @PutMapping("/users")
    public void updateUser(@Valid @RequestBody User user){
        //userService.saveUser(user);
        userRepository.updateUser(user);
    }
    
    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable(value = "id") Long idUser){
        userRepository.deleteUser(idUser);
        //userService.deleteUser(idUser);
    }
}
