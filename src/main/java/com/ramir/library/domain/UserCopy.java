/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.domain;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.persistence.*;
import lombok.Data;

/**
 *
 * @author ramir
 */

@Data
public class UserCopy implements Serializable{
    public static final Long serialVersionUID = 1L;

    private Long id;
    

    private Copy copy;
    
    private User user;

    private Date borrowDate;
   
    private Date returnDate;
}
