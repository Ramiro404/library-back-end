/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.dao;

import com.ramir.library.domain.User;
import java.util.List;

/**
 *
 * @author ramir
 */
public interface UserDao {
     public List<User> getAllUsers();
     public User getUserById(Long idUser);
     public User updateUser(User user);
     public void createUser(User user);
     public void deleteUser(Long idUser);
}
