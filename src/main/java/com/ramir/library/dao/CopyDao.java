/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.dao;

import com.ramir.library.domain.Copy;
import java.util.List;

/**
 *
 * @author ramir
 */
public interface CopyDao{
    public List<Copy> getAllCopies();
    public Copy getCopyById(Long idCopy);
    public void updateCopy(Copy copy);
    public void createCopy(Copy copy);
    public void deleteCopy(Long idCopy);
}
