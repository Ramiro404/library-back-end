
package com.ramir.library.domain;

/**
 *
 * @author ramir
 */
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;

@Data

public class Book implements Serializable{
    public static final Long serialVersionUID = 1L;
    

    private Long idBook;
    private String title;
    
    private String bookPath;
    private String image;
    
    private List<Review> reviews;
    
    private List<Edition> editions;
    

    private Set<Genre> bookGenres;
    
    private Set<Author> authors;
    

    private Set<Book> referencedBooks;
    
    
    
}
