/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.repository;

import com.ramir.library.dao.CopyDao;
import com.ramir.library.domain.Copy;
import com.ramir.library.mappers.CopyMapper;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author ramir
 */
public class CopyRepository implements CopyDao{
    
    private JdbcTemplate jdbcTemplate;

    public CopyRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    @Override
    public List<Copy> getAllCopies() {
        String SQL = "select id_copy,number from copies" ;
        return jdbcTemplate.query(SQL, new CopyMapper());
    }

    @Override
    public Copy getCopyById(Long idCopy) {
        String SQL = "select id_copy,number from copies where id_copy=?";
        return jdbcTemplate.queryForObject(SQL, new CopyMapper(), idCopy);
    }

    @Override
    public void updateCopy(Copy copy) {
        String SQL = "update copies set number=? where id_copy=?";
        jdbcTemplate.update(SQL, copy.getNumber(), copy.getIdCopy());
    }

    @Override
    public void createCopy(Copy copy) {
        String SQL = "insert into copies(number) values(?)";
        jdbcTemplate.update(SQL, copy.getNumber(),copy.getIdCopy());
    }

    @Override
    public void deleteCopy(Long idCopy) {
        String SQL = "delete from copies where id_copy=?";
        jdbcTemplate.update(SQL, idCopy);
    }
    
}
