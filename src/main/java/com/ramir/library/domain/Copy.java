
package com.ramir.library.domain;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import lombok.Data;
/**
 *
 * @author ramir
 */
@Data
public class Copy implements Serializable{
    public static final long serialVersionUID = 1L;
    

    private Long idCopy;
    private Long number;
    //private Edition edition;
}
