/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.mappers;

import com.ramir.library.domain.Copy;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author ramir
 */
public class CopyMapper implements RowMapper<Copy>{

    @Override
    public Copy mapRow(ResultSet rs, int i) throws SQLException {
        Copy copy = new Copy();
        copy.setIdCopy(rs.getLong("id_copy"));
        copy.setNumber(rs.getLong("number"));
        return copy;
    }
    
}
