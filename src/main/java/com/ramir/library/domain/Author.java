/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.domain;

import java.io.Serializable;
import java.time.Year;
import java.util.List;
import java.util.Set;
import javax.persistence.*;
import lombok.Data;

/**
 *
 * @author ramir
 */

public class Author implements Serializable{
    
    public static final Long serialVersionUID = 1L;

    private Long idAuthor;
    
    private String name;
    
    private String lastName;
    
    private Year yearBirth;
    

    private Set<Book> authorBooks;

    private Country country;
}
