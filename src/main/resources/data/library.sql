-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema library
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema library
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `library` DEFAULT CHARACTER SET utf8 ;
USE `library` ;

-- -----------------------------------------------------
-- Table `library`.`books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`books` (
  `id_book` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(600) NOT NULL,
  PRIMARY KEY (`id_book`),
  UNIQUE INDEX `id_book_UNIQUE` (`id_book` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`countries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`countries` (
  `id_country` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `nacionality` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_country`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`authors` (
  `id_author` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(300) NOT NULL,
  `year_birth` YEAR(4) NOT NULL,
  `id_country` INT NOT NULL,
  PRIMARY KEY (`id_author`),
  INDEX `fk_authors_countries1_idx` (`id_country` ASC),
  CONSTRAINT `fk_authors_countries1`
    FOREIGN KEY (`id_country`)
    REFERENCES `library`.`countries` (`id_country`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`books_authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`books_authors` (
  `id_book` INT NOT NULL,
  `id_author` INT NOT NULL,
  PRIMARY KEY (`id_book`, `id_author`),
  INDEX `fk_Book_has_author_author1_idx` (`id_author` ASC),
  INDEX `fk_Book_has_author_Book_idx` (`id_book` ASC),
  CONSTRAINT `fk_Book_has_author_Book`
    FOREIGN KEY (`id_book`)
    REFERENCES `library`.`books` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Book_has_author_author1`
    FOREIGN KEY (`id_author`)
    REFERENCES `library`.`authors` (`id_author`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`referenced_books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`referenced_books` (
  `id_book` INT NOT NULL,
  `id_referenced_book` INT NOT NULL,
  PRIMARY KEY (`id_book`, `id_referenced_book`),
  INDEX `fk_books_has_books_books2_idx` (`id_referenced_book` ASC),
  INDEX `fk_books_has_books_books1_idx` (`id_book` ASC),
  CONSTRAINT `fk_books_has_books_books1`
    FOREIGN KEY (`id_book`)
    REFERENCES `library`.`books` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_books_has_books_books2`
    FOREIGN KEY (`id_referenced_book`)
    REFERENCES `library`.`books` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`reviews` (
  `id_review` INT NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(500) NOT NULL,
  `date` DATE NOT NULL,
  `magazine_name` VARCHAR(300) NOT NULL,
  `id_book` INT NOT NULL,
  PRIMARY KEY (`id_review`),
  INDEX `fk_reviews_books1_idx` (`id_book` ASC),
  CONSTRAINT `fk_reviews_books1`
    FOREIGN KEY (`id_book`)
    REFERENCES `library`.`books` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`genres` (
  `id_genre` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id_genre`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`books_genres`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`books_genres` (
  `id_book` INT NOT NULL,
  `id_genre` INT NOT NULL,
  PRIMARY KEY (`id_book`, `id_genre`),
  INDEX `fk_books_has_genres_genres1_idx` (`id_genre` ASC),
  INDEX `fk_books_has_genres_books1_idx` (`id_book` ASC),
  CONSTRAINT `fk_books_has_genres_books1`
    FOREIGN KEY (`id_book`)
    REFERENCES `library`.`books` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_books_has_genres_genres1`
    FOREIGN KEY (`id_genre`)
    REFERENCES `library`.`genres` (`id_genre`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`editions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`editions` (
  `id_edition` INT NOT NULL AUTO_INCREMENT,
  `isbn` VARCHAR(100) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `year` YEAR(4) NOT NULL,
  `id_book` INT NOT NULL,
  PRIMARY KEY (`id_edition`),
  UNIQUE INDEX `isbn_UNIQUE` (`isbn` ASC),
  INDEX `fk_editions_books1_idx` (`id_book` ASC),
  CONSTRAINT `fk_editions_books1`
    FOREIGN KEY (`id_book`)
    REFERENCES `library`.`books` (`id_book`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`copies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`copies` (
  `id_copy` INT NOT NULL AUTO_INCREMENT,
  `number` INT NOT NULL,
  `id_edition` INT NOT NULL,
  PRIMARY KEY (`id_copy`),
  INDEX `fk_copies_editions1_idx` (`id_edition` ASC),
  CONSTRAINT `fk_copies_editions1`
    FOREIGN KEY (`id_edition`)
    REFERENCES `library`.`editions` (`id_edition`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`users` (
  `dni_user` INT NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`dni_user`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`users_copies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`users_copies` (
  `dni_user` INT NOT NULL,
  `id_copy` INT NOT NULL,
  `borrow_date` DATE NOT NULL,
  `return_date` DATE NOT NULL,
  PRIMARY KEY (`dni_user`, `id_copy`),
  INDEX `fk_users_has_copies_copies1_idx` (`id_copy` ASC),
  INDEX `fk_users_has_copies_users1_idx` (`dni_user` ASC),
  CONSTRAINT `fk_users_has_copies_users1`
    FOREIGN KEY (`dni_user`)
    REFERENCES `library`.`users` (`dni_user`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_copies_copies1`
    FOREIGN KEY (`id_copy`)
    REFERENCES `library`.`copies` (`id_copy`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `library`.`languages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `library`.`languages` (
  `id_language` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `id_edition` INT NOT NULL,
  PRIMARY KEY (`id_language`),
  INDEX `fk_languages_editions1_idx` (`id_edition` ASC),
  CONSTRAINT `fk_languages_editions1`
    FOREIGN KEY (`id_edition`)
    REFERENCES `library`.`editions` (`id_edition`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
