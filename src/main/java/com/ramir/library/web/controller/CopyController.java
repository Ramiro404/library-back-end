/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ramir.library.web.controller;

import com.ramir.library.domain.Copy;
import com.ramir.library.repository.CopyRepository;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ramir
 */
@RestController
@RequestMapping(value = "/api")
public class CopyController {
    
    @Autowired
    private CopyRepository copyRepository;
    
    @GetMapping("/copies")
    public List<Copy> getCopies(){
        return copyRepository.getAllCopies();
    }
    
    @GetMapping("/copies/{id}")
    public Copy getCopyById(@PathVariable(value = "id") Long idCopy){
        return copyRepository.getCopyById(idCopy);
    }
    
    @PostMapping("/copies")
    public void createCopy(@Valid @RequestBody Copy copy){
        copyRepository.createCopy(copy);
    }
    
    @PutMapping("/copies")
    public void updateCopy(@Valid @RequestBody Copy copy){
        copyRepository.updateCopy(copy);
    }
    
    @DeleteMapping("/copies/{id}")
    public void deleteCopy(@PathVariable(value = "id") Long idCopy){
        copyRepository.deleteCopy(idCopy);
    }
            
}
