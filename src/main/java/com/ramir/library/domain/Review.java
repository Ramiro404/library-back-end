
package com.ramir.library.domain;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.*;
import lombok.Data;

/**
 *
 * @author ramir
 */
@Data

public class Review implements Serializable{
    public static final Long serialVerionUID = 1L;
    

    private Long idReview;
    private String description;
    private Date date;
    
    private String magazineName;
    

    private Book book;
}
